/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('smoke test', () => {
    it('does not smoke', () => {
        cy.visit('localhost:8000/')

        cy.log('login')
        cy.get('#id_login').type('foo')
        cy.get('#id_password').type('Fn5CRh2!kCG!9cj')
        cy.get('.primaryAction').click()

        cy.log('see keyword result')
        cy.get('[data-cy="key-list-link"]').first().click()

        cy.log('upload keywords')
        cy.get('[data-cy="upload-page"]').click()
        cy.get('[data-cy="file-browse"]').attachFile('keywords.csv')
        cy.get('[data-cy="upload-btn"]').click()
    })
})

