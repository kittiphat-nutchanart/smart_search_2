from django.urls import path
from django.shortcuts import render
from faker import Faker
import random
fake = Faker()


def search_list(request):
    def create_keyword():
        keyword = {
            'keyword': fake.text(max_nb_chars=20),
            'search_status': random.choice(['IN_PROGRESS', 'COMPLETE'])
        }
        return keyword

    keywords = [create_keyword() for i in range(10)]
    props = { 
        'keywords': keywords, 
        'page': random.randrange(1, 5),
        'has_more': random.choice([True, False])
    }
    return render(request, 'pages/search_list.html', props)

def search_detail(request, keyword):
    props = { 
        'keyword': keyword,
        'adwords_count': random.randrange(1, 1000),
        'links_count': random.randrange(1, 1000),
        'total_results': random.randrange(1, 1000),
        'html': fake.paragraph(nb_sentences=5),
        'search_status': 'INP'
    }
    return render(request, 'pages/search_detail.html', props)

app_name = "fake"
urlpatterns = [
    path("searches/", view=search_list),
    path("searches/<keyword>/", view=search_detail),
]

