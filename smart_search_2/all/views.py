from django.shortcuts import render, redirect
from smart_search_2.all.models import SearchKeyword
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.views import View
from config import celery_app
from django.db import transaction
import requests 
import re
from bs4 import BeautifulSoup


def extract_google(q):
    headers_Get = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-language": "en-US,en;q=0.9",
        "cache-control": "max-age=0",
        "preferanonymous": "1",
        "sec-ch-dpr": "1",
        "sec-ch-ua": "\" Not;A Brand\";v=\"99\", \"Chromium\";v=\"103\", \"Google Chrome\";v=\"103.0.1264.44\"",
        "sec-ch-ua-arch": "\"x86\"",
        "sec-ch-ua-bitness": "\"64\"",
        "sec-ch-ua-full-version": "\"103.0.1264.44\"",
        "sec-ch-ua-full-version-list": "\" Not;A Brand\";v=\"99.0.0.0\", \"Microsoft Edge\";v=\"103.0.1264.44\", \"Chromium\";v=\"103.0.5060.66\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-model": "\"\"",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-ch-ua-platform-version": "\"14.0.0\"",
        "sec-ch-ua-wow64": "?0",
        "sec-ch-viewport-width": "1366",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "cookie": "AEC=AakniGODfpks8CtoiiarZR9JVWhNy6eTsaEgpjcPWAsvLwM7oHSOWeYcaA; 1P_JAR=2022-07-05-00; NID=511=izCilzukIZo0C-X1FDTV8bh3RjaNwEjWkR-uD6wwwEVZXFHEcsZFDstsBH7zgP_ifI6_g7LZMVbSp0iiHLb7jDyZjRv59BREFR470YAybHdn_t9IM-lNr1m1IFk4XVnXrJ9TJkQmoZ15DHsNiuz9oaNs4pgBQBsLzvAB2--qycbBt71Jma5DmwKV3qyPamS7JU3nmG219qs; DV=8zdt1qEnzlcuQL5Nu0W6IhdZll-8HFiMYpTpdNrEqQAAAAA",
        "Referer": "https://www.google.com/",
        "Referrer-Policy": "origin",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.66 Safari/537.36"
    }
    s = requests.Session()
    q = '+'.join(q.split())
    url = "https://www.google.com/search?q=" + q + "&ei=jjfDYvG8Gs714-EP0a6F8Ao&oq=%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%95%E0%B8%B5&gs_lcp=Cgdnd3Mtd2l6EAMYADIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzIECAAQEzoHCAAQRxCwAzoECAAQAzoFCAAQgAQ6BAguEAM6EQgAEOoCELQCEIoDELcDEOUCOg4ILhCPARDqAhCMAxDlAjoOCAAQjwEQ6gIQjAMQ5QI6BQguEIAEOgQIABAeSgQIQRgASgQIRhgAUJoFWPB5YM6DAWgJcAF4AIABTogB_QSSAQE5mAEAoAEBsAEKyAEIwAEB&sclient=gws-wiz"
    r = s.get(url, headers=headers_Get)

    total_results = None
    try:
        soup = BeautifulSoup(r.text, "html.parser")
        total_results_tag = soup.select_one("div#result-stats")
        total_results_tag.nobr.extract()
        total_results_0 = total_results_tag.get_text()
        total_results_1 = re.sub("[^0-9]", "", total_results_0)
        total_results = int(total_results_1)
    except Exception as e:
        pass

    adwords_count = len(soup.select("div[data-text-ad]"))
    links_count = len(soup.select("a[href]"))

    result = {
        'adwords_count': adwords_count,
        'links_count': links_count,
        'total_results': total_results,
        'html': r.text
    }

    return result

@celery_app.task(rate_limit='5/s', default_retry_delay=10)
def google_and_save(keyword, id):
    try:
        extract_result = extract_google(keyword)
        key = SearchKeyword.objects.get(pk=id)
        key.adwords_count = extract_result['adwords_count']
        key.links_count = extract_result['links_count']
        key.total_search_results = extract_result['total_results']
        key.raw_html = extract_result['html']
        key.search_status = 'COM'
        key.save()
    except Exception as exc:
        raise self.retry(exc=exc)


class UploadKeywordsForm(forms.Form):
    file = forms.FileField()

class UploadView(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect('/accounts/login')

        return render(request, 'pages/upload.html')

    def post(self, request):
        if not request.user.is_authenticated:
            return redirect('/accounts/login')

        form = UploadKeywordsForm(request.POST, request.FILES)

        if not form.is_valid():
            return render(request, '400.html')
        
        file = request.FILES['file']
        with file.open() as f:
            lines = f.readlines()

            if len(lines) > 100:
                return render(request, '400.html')

            def make_keyword(keyword):
                formated_key = keyword.strip().decode('utf-8')
                raw_keyword = SearchKeyword(
                    keyword=formated_key,
                    search_status='INP',
                    adwords_count=None,
                    total_search_results=None,
                    uploader=request.user,
                    raw_html=''
                )
                return raw_keyword

            raw_keywords = [make_keyword(key) for key in lines]
            keywords = SearchKeyword.objects.bulk_create(raw_keywords)

            def execute_tasks():
                for keyword in keywords:
                    google_and_save.delay(keyword.keyword, keyword.id)
            
            transaction.on_commit(execute_tasks)

        return redirect('/all/smart-search/page-1')

def search_list(request, page):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    
    limit = 20
    offset = (page - 1) * limit
    stop = offset + limit
    keywords = SearchKeyword.objects.filter(uploader_id=request.user.id).order_by('-id')[offset:stop]

    props = {
        'keywords': keywords,
        'page': page
    }

    return render(request, 'pages/search_list.html', props)

def search_detail(request, id):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    
    try:
        keyword = SearchKeyword.objects.get(id=id)
    except ObjectDoesNotExist:
        return render(request, '404.html')
    
    if keyword.uploader.id != request.user.id:
        return render(request, '403.html')

    props = {
        'keyword': keyword.keyword,
        'adwords_count': keyword.adwords_count,
        'links_count': keyword.links_count,
        'total_results': keyword.total_search_results,
        'html': keyword.raw_html,
        'search_status': keyword.search_status
    }

    return render(request, 'pages/search_detail.html', props)


class InternalSearchView(View):
    def get(self, request, term):
        if not request.user.is_authenticated:
            return redirect('/accounts/login')

        keywords = SearchKeyword.objects.filter(keyword__search=term).order_by('-id')[:100]

        props = {
            'keywords': keywords,
            'term': term
        }

        return render(request, 'pages/internal_search.html', props)