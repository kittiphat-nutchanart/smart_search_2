from django.urls import path

from smart_search_2.all.views import (
    UploadView,
    search_list,
    search_detail,
    InternalSearchView
)

app_name = "all"
urlpatterns = [
    path("upload/", view=UploadView.as_view(), name="upload"),
    path("smart-search/page-<int:page>", view=search_list, name="search_list"),
    path("smart-search/keyword/<int:id>", view=search_detail, name="search_detail"),
    path("internal/search/<str:term>", view=InternalSearchView.as_view(), name="internal_search"),
]
