from django.db import models
from smart_search_2.users.models import User

class SearchKeyword(models.Model):

    class SearchStatus(models.TextChoices):
        IN_PROGRESS = 'INP'
        COMPLETE = 'COM'

    keyword = models.TextField()
    search_status = models.TextField(choices=SearchStatus.choices)
    adwords_count = models.IntegerField(null=True)
    links_count = models.IntegerField(null=True)
    total_search_results = models.BigIntegerField(null=True)
    uploader = models.ForeignKey(User, on_delete=models.CASCADE)
    raw_html = models.TextField()
