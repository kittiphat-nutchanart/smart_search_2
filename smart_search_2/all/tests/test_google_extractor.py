import pytest
import requests 
from smart_search_2.all.views import extract_google

pytestmark = pytest.mark.django_db


def test_extract_google_search():
    result = extract_google('Phuket hotel')

    assert isinstance(result['total_results'], int)
    assert isinstance(result['links_count'], int)
    assert isinstance(result['adwords_count'], int)
    assert isinstance(result['html'], str)
