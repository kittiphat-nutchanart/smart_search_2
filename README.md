# smart_search_2

Behold My Awesome Project!

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: MIT

## run app locally

-   make sure you have docker and then run

        $ docker-compose -f local.yml up -d 

-   the app is now running on localhost:8000

-   after signup confirm your email on mailbox running on localhost:8025. your confirmation email should be in there

## run tests

### pytest   

        $ docker-compose -f local.yml run django pytest

### Cypress
cypress tests require that the app is running locally. follow these steps to run cypress tests
-   1.) First make sure that the app is running locally (see above)

-   2.) then load appropiate data for the test

        $ docker-compose -f local.yml run django /entrypoint sh -c "python manage.py flush --no-input && python manage.py loaddata fixtures/e2e.yaml"

-   3.) finally install cypress and run tests

        $ cd e2e
        $ npm i
        $ npx run cypress